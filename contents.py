import pathlib


def iter_current_folder(path, content_list):
    for file_or_dir in pathlib.Path(path).iterdir():
        if str(file_or_dir)[-3:] == 'txt' and str(file_or_dir) != 'contents.txt':
            with open(file_or_dir, 'r') as f:
                content_list.append(f.read())
        if file_or_dir.is_dir():
            iter_current_folder(file_or_dir, content_list)

    return content_list


output = iter_current_folder('.', [])

with open('contents.txt', 'w') as nf:
    nf.write("\n========\n".join(sorted(output))
